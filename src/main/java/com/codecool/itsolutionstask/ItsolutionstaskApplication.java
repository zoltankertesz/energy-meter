package com.codecool.itsolutionstask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItsolutionstaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(ItsolutionstaskApplication.class, args);
    }

}
