package com.codecool.itsolutionstask.controller;

import com.codecool.itsolutionstask.dto.YearlyReadingsInMonthsDto;
import com.codecool.itsolutionstask.service.ReadingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class ReadingController {

    @Autowired
    private ReadingService readingService;

    @GetMapping("/readings/monthsbyyear")
    public YearlyReadingsInMonthsDto getMonthlySumByYear(@RequestParam("year") int year) {
        return readingService.getMonthlySumOfReadingsByYear(year);
    }

    @GetMapping("/readings/singlemonth")
    public ResponseEntity getSumOfSingleMonthByYearAndMonth(@RequestParam("year") int year,
                                                            @RequestParam("month") int month) {
        return readingService.getSumOfReadingsByYearAndMonth(year, month);
    }

    @GetMapping("/readings/yearlysum")
    public List<Map> getYearlySum(@RequestParam("year") int year) {
        return readingService.getSumOfReadingsByYear(year);
    }

    @PostMapping("/readings")
    public ResponseEntity addReading(@RequestParam("year") int year,
                                     @RequestParam("month") int month,
                                     @RequestParam("value") int readingValue,
                                     @RequestParam("meterid") int meterId) {
        return readingService.addReading(year, month, readingValue, meterId);
    }
}
