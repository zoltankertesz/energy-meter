package com.codecool.itsolutionstask.controller;

import com.codecool.itsolutionstask.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientController {

    @Autowired
    private ClientService clientService;

    @PostMapping("/clients")
    public void addClient(@RequestParam("clientname") String clientName,
                          @RequestParam("identitycardnumber") String identityCardNumber,
                          @RequestParam("city") String city,
                          @RequestParam("zipcode") int zipCode,
                          @RequestParam("street") String street,
                          @RequestParam("housenumber") int houseNumber) {
        clientService.addNewClient(clientName, identityCardNumber, city, zipCode, street, houseNumber);
    }
}
