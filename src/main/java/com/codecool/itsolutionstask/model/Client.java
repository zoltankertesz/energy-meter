package com.codecool.itsolutionstask.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Clients")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @Column(name = "identity_card_number", unique = true)
    private String identityCardNumber;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "meter_id", referencedColumnName = "id")
    private Meter meter;

    public Client() {
    }

    public Client(String name, String identityCardNumber, Address address, Meter meter) {
        this.name = name;
        this.identityCardNumber = identityCardNumber;
        this.address = address;
        this.meter = meter;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getIdentityCardNumber() {
        return identityCardNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Meter getMeter() {
        return meter;
    }

    public void setMeter(Meter meter) {
        this.meter = meter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(name, client.name) &&
                Objects.equals(identityCardNumber, client.identityCardNumber);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, identityCardNumber);
    }
}
