package com.codecool.itsolutionstask.model;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.util.Objects;

@Entity
@Table(name = "Readings")
public class Reading {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private int year;
    private String month;
    @Min(value = 0)
    @Column(nullable = false)
    private int value;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "meter_id", referencedColumnName = "id")
    private Meter meter;

    public Reading(int year, String month, int value, Meter meter) {
        this.year = year;
        this.month = month;
        this.value = value;
        this.meter = meter;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Meter getMeter() {
        return meter;
    }

    public void setMeter(Meter meter) {
        this.meter = meter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reading reading = (Reading) o;
        return year == reading.year &&
                value == reading.value &&
                Objects.equals(id, reading.id) &&
                Objects.equals(month, reading.month) &&
                Objects.equals(meter, reading.meter);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, year, month, value, meter);
    }
}
