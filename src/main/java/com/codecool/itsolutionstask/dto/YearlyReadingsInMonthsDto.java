package com.codecool.itsolutionstask.dto;

import java.util.List;
import java.util.Objects;

public class YearlyReadingsInMonthsDto {

    private int year;
    private List<Integer> months;


    public YearlyReadingsInMonthsDto(int year, List<Integer> months) {
        this.year = year;
        this.months = months;
    }

    public int getYear() {
        return year;
    }

    public int getJanuary() {
        return months.get(0);
    }

    public int getFebruary() {
        return months.get(1);
    }

    public int getMarch() {
        return months.get(2);
    }

    public int getApril() {
        return months.get(3);
    }

    public int getMay() {
        return months.get(4);
    }

    public int getJune() {
        return months.get(5);
    }

    public int getJuly() {
        return months.get(6);
    }

    public int getAugust() {
        return months.get(7);
    }

    public int getSeptember() {
        return months.get(8);
    }

    public int getOctober() {
        return months.get(9);
    }

    public int getNovember() {
        return months.get(10);
    }

    public int getDecember() {
        return months.get(11);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        YearlyReadingsInMonthsDto that = (YearlyReadingsInMonthsDto) o;
        return year == that.year &&
                Objects.equals(months, that.months);
    }

    @Override
    public int hashCode() {

        return Objects.hash(year, months);
    }
}
