package com.codecool.itsolutionstask.repository;

import com.codecool.itsolutionstask.model.Reading;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ReadingRepository extends JpaRepository<Reading, Integer> {

    @Query(value = "SELECT SUM(value) FROM Readings WHERE year = ?", nativeQuery = true)
    Integer findSumOfReadingsByYear(int year);

    @Query(value = "SELECT SUM(value) FROM Readings WHERE year = ?1 AND month= ?2", nativeQuery = true)
    Integer findSumOfReadingsByYearAndMonth(int year, String month);

    boolean existsByYearAndMonthAndMeterId(int year, String month, int meterId);

}
