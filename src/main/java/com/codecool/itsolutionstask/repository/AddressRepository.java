package com.codecool.itsolutionstask.repository;

import com.codecool.itsolutionstask.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {

    Address findByZipCodeAndCityAndStreetAndHouseNumber(int zipCode, String city, String street, int houseNumber);
}
