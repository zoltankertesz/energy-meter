package com.codecool.itsolutionstask.repository;

import com.codecool.itsolutionstask.model.Meter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MeterRepository extends JpaRepository<Meter, Integer> {

    Meter findById(int id);

}
