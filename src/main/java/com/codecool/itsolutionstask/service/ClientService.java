package com.codecool.itsolutionstask.service;

import com.codecool.itsolutionstask.model.Address;
import com.codecool.itsolutionstask.model.Client;
import com.codecool.itsolutionstask.model.Meter;
import com.codecool.itsolutionstask.repository.AddressRepository;
import com.codecool.itsolutionstask.repository.ClientRepository;
import com.codecool.itsolutionstask.repository.MeterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;


@Component
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private MeterRepository meterRepository;

    public void addNewClient(String clientName, String identityCardNumber, String city, int zipCode, String street, int houseNumber) {
        if (clientRepository.findByIdentityCardNumber(identityCardNumber) != null) {
            throw new IllegalArgumentException("Client already registered in database.");
        }

        Address address = addressRepository.findByZipCodeAndCityAndStreetAndHouseNumber(zipCode, city, street, houseNumber);
        if (address != null) {
            Client client = clientRepository.findByAddressId(address.getId());
            if (client != null) {
                throw new IllegalArgumentException("Address already used by a client.");
            } else {
                String meterName = UUID.randomUUID().toString().replace("-", "");
                Meter meter = new Meter(meterName, address);
                meterRepository.save(meter);
                Client newClient = new Client(clientName, identityCardNumber, address, meter);
                clientRepository.save(newClient);
            }

        } else {
            Address newAddress = new Address(zipCode, city, street, houseNumber);
            addressRepository.save(newAddress);
            String meterName = UUID.randomUUID().toString().replace("-", "");
            Meter meter = new Meter(meterName, newAddress);
            meterRepository.save(meter);
            Client newClient = new Client(clientName, identityCardNumber, newAddress, meter);
            clientRepository.save(newClient);
        }


    }
}
