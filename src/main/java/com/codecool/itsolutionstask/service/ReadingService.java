package com.codecool.itsolutionstask.service;

import com.codecool.itsolutionstask.dto.YearlyReadingsInMonthsDto;
import com.codecool.itsolutionstask.model.Meter;
import com.codecool.itsolutionstask.model.Reading;
import com.codecool.itsolutionstask.repository.MeterRepository;
import com.codecool.itsolutionstask.repository.ReadingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.text.DateFormatSymbols;
import java.util.*;

@Component
public class ReadingService {

    @Autowired
    private ReadingRepository readingRepository;

    @Autowired
    private MeterRepository meterRepository;

    public List<Map> getSumOfReadingsByYear(int year) {
        Integer readingsSum = readingRepository.findSumOfReadingsByYear(year);

        Map<String, Integer> yearMap = new HashMap<>();
        yearMap.put("year", year);

        if (readingsSum == null) {
            readingsSum = 0;
        }
        Map<String, Integer> totalMap = new HashMap<>();
        totalMap.put("total", readingsSum);

        List<Map> readings = new ArrayList<>();
        readings.add(yearMap);
        readings.add(totalMap);
        return readings;
    }

    public YearlyReadingsInMonthsDto getMonthlySumOfReadingsByYear(int year) {
        List<String> months = Arrays.asList(new DateFormatSymbols(Locale.ENGLISH).getMonths());
        List<Integer> values = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            Integer value = readingRepository.findSumOfReadingsByYearAndMonth(year, months.get(i));
            if (value == null) {
                value = 0;
            }
            values.add(value);
        }

        return new YearlyReadingsInMonthsDto(year, values);
    }

    public ResponseEntity getSumOfReadingsByYearAndMonth(int year, int month) {
        if (month > 12 || month < 1) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        List<String> months = Arrays.asList(new DateFormatSymbols(Locale.ENGLISH).getMonths());
        String monthString = months.get(month - 1);

        Integer reading = readingRepository.findSumOfReadingsByYearAndMonth(year, monthString);
        if (reading == null) {
            reading = 0;
        }

        Map<String, Integer> yearMap = new HashMap<>();
        yearMap.put("year", year);

        Map<String, Integer> monthMap = new HashMap<>();
        monthMap.put(monthString, reading);

        List<Map> readings = new ArrayList<>();
        readings.add(yearMap);
        readings.add(monthMap);
        return new ResponseEntity<List>(readings, HttpStatus.OK);
    }

    public ResponseEntity addReading(int year, int month, int readingValue, int meterId) {
        if (month > 12 || month < 1) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        Meter meter = meterRepository.findById(meterId);

        if (meter == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        List<String> months = Arrays.asList(new DateFormatSymbols(Locale.ENGLISH).getMonths());
        String monthString = months.get(month - 1);

        if (readingRepository.existsByYearAndMonthAndMeterId(year, monthString, meterId)) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("Reading data for meter already exists on given date.");
        }


        Reading newReading = new Reading(year, monthString, readingValue, meter);
        readingRepository.save(newReading);
        return new ResponseEntity(HttpStatus.OK);
    }
}
