package com.codecool.itsolutionstask;

import com.codecool.itsolutionstask.controller.ReadingController;
import com.codecool.itsolutionstask.dto.YearlyReadingsInMonthsDto;
import com.codecool.itsolutionstask.service.ReadingService;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ReadingController.class)
@RunWith(SpringRunner.class)
public class ReadingControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ReadingService service;


    @Test
    public void shouldReturnYearlySum() throws Exception {
        List<Map> testList = new ArrayList<>();
        Map<String, Integer> map1 = new HashMap<>();
        Map<String, Integer> map2 = new HashMap<>();
        map1.put("year", 2018);
        map2.put("total", 1611);
        testList.add(map1);
        testList.add(map2);
        when(service.getSumOfReadingsByYear(2018)).thenReturn(testList);

        mockMvc.perform(get("/readings/yearlysum?year={year}", 2018))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json("[{\"year\":2018},{\"total\":1611}]"));
    }

    @Test
    public void shouldReturnYearlyReadingsMonthly() throws Exception {
        YearlyReadingsInMonthsDto testItem = new YearlyReadingsInMonthsDto(2015, new ArrayList<>(
                Arrays.asList(30, 35, 20, 15, 24, 34, 42, 32, 13, 54, 43, 12)));
        when(service.getMonthlySumOfReadingsByYear(2015)).thenReturn(testItem);

        mockMvc.perform(get("/readings/monthsbyyear?year={year}", 2015))
                .andExpect(status().isOk())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.year").value(2015))
                .andExpect(jsonPath("$.january").value(30))
                .andExpect(jsonPath("$.december").value(12));
    }

    @Test
    public void shouldReturnSpecific() throws Exception {
        List<Map> testMapList = new ArrayList<>();
        Map<String, Integer> map1 = new HashMap<>();
        Map<String, Integer> map2 = new HashMap<>();
        map1.put("year", 2019);
        map2.put("May", 55);
        testMapList.add(map1);
        testMapList.add(map2);
        when(service.getSumOfReadingsByYearAndMonth(2019, 5)).thenReturn(new ResponseEntity<List>(testMapList, HttpStatus.OK));

        mockMvc.perform(get("/readings/singlemonth?year={year}&month={month}", 2019, 5))
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"year\":2019},{\"May\":55}]"))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0]", Matchers.hasEntry("year", 2019)))
                .andExpect(jsonPath("$[1]", hasEntry("May", 55)));
    }

    @Test
    public void shouldReturnEmptyJson() throws Exception {
        mockMvc.perform(get("/readings/yearlysum?year={year}", 2017))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    @Test
    public void shouldReturnBadRequest() throws Exception {
        String strInp = "stringThatDoesn'tBelongHere";
        mockMvc.perform(get("/readings/yearlysum?year={year}", strInp)).andExpect(status().isBadRequest());
        mockMvc.perform(get("/readings/singlemonth?year={year}&month={month}", strInp, 12)).andExpect(status().isBadRequest());
        mockMvc.perform(get("/readings/singlemonth?year={year}&month={month}", 2018, strInp)).andExpect(status().isBadRequest());
        mockMvc.perform(get("/readings/monthsbyyear?year={year}", strInp)).andExpect(status().isBadRequest());
    }


}
