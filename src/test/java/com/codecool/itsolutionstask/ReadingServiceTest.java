package com.codecool.itsolutionstask;

import com.codecool.itsolutionstask.dto.YearlyReadingsInMonthsDto;
import com.codecool.itsolutionstask.repository.MeterRepository;
import com.codecool.itsolutionstask.repository.ReadingRepository;
import com.codecool.itsolutionstask.service.ReadingService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


@RunWith(SpringRunner.class)
public class ReadingServiceTest {

    @Autowired
    private ReadingService readingService;
    @MockBean
    private ReadingRepository readingRepository;
    @MockBean
    private MeterRepository meterRepository;

    @Before
    public void setUp() {
        int year = 2019;
        int meterId = 1;

        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(year, "January")).thenReturn(30);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(year, "February")).thenReturn(35);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(year, "March")).thenReturn(20);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(year, "April")).thenReturn(15);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(year, "May")).thenReturn(50);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(year, "June")).thenReturn(34);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(year, "July")).thenReturn(42);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(year, "August")).thenReturn(32);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(year, "September")).thenReturn(13);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(year, "October")).thenReturn(54);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(year, "November")).thenReturn(43);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(year, "December")).thenReturn(12);

        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(2021, "January")).thenReturn(0);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(2021, "February")).thenReturn(0);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(2021, "March")).thenReturn(0);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(2021, "April")).thenReturn(0);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(2021, "May")).thenReturn(0);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(2021, "June")).thenReturn(0);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(2021, "July")).thenReturn(0);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(2021, "August")).thenReturn(0);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(2021, "September")).thenReturn(0);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(2021, "October")).thenReturn(0);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(2021, "November")).thenReturn(0);
        Mockito.when(readingRepository.findSumOfReadingsByYearAndMonth(2021, "December")).thenReturn(0);

        Mockito.when(readingRepository.findSumOfReadingsByYear(year)).thenReturn(50);

        Mockito.when(readingRepository.existsByYearAndMonthAndMeterId(year, "May", meterId)).thenReturn(true);

        Mockito.when(readingRepository.findSumOfReadingsByYear(2021)).thenReturn(null);

    }

    @Test
    public void sumOfReadingsByYearWhenExistingYear() {
        int year = 2019;
        int readingValue = 50;
        Map<String, Integer> testMap1 = new HashMap<>();
        testMap1.put("year", year);
        Map<String, Integer> testMap2 = new HashMap<>();
        testMap2.put("total", readingValue);

        List<Map> testMapList = Arrays.asList(testMap1, testMap2);
        List<Map> listReturned = readingService.getSumOfReadingsByYear(year);

        assertThat(testMapList, equalTo(listReturned));
    }

    @Test
    public void monthlySumWhenExistingYear() {
        int year = 2019;
        YearlyReadingsInMonthsDto testYRIMD = new YearlyReadingsInMonthsDto(year, new ArrayList<>(
                Arrays.asList(30, 35, 20, 15, 50, 34, 42, 32, 13, 54, 43, 12)));
        YearlyReadingsInMonthsDto returnedYRIMD = readingService.getMonthlySumOfReadingsByYear(year);

        assertThat(testYRIMD, equalTo(returnedYRIMD));
        assertThat(testYRIMD.getYear(), equalTo(returnedYRIMD.getYear()));
        assertThat(testYRIMD.getJanuary(), equalTo(returnedYRIMD.getJanuary()));
        assertThat(testYRIMD.getFebruary(), equalTo(returnedYRIMD.getFebruary()));
        assertThat(testYRIMD.getMarch(), equalTo(returnedYRIMD.getMarch()));
        assertThat(testYRIMD.getApril(), equalTo(returnedYRIMD.getApril()));
        assertThat(testYRIMD.getMay(), equalTo(returnedYRIMD.getMay()));
        assertThat(testYRIMD.getJune(), equalTo(returnedYRIMD.getJune()));
        assertThat(testYRIMD.getJuly(), equalTo(returnedYRIMD.getJuly()));
        assertThat(testYRIMD.getAugust(), equalTo(returnedYRIMD.getAugust()));
        assertThat(testYRIMD.getSeptember(), equalTo(returnedYRIMD.getSeptember()));
        assertThat(testYRIMD.getOctober(), equalTo(returnedYRIMD.getOctober()));
        assertThat(testYRIMD.getNovember(), equalTo(returnedYRIMD.getNovember()));
        assertThat(testYRIMD.getDecember(), equalTo(returnedYRIMD.getDecember()));
    }

    @Test
    public void sumOfReadingsByYearAndMonthWhenExistingYearAndMonth() {
        int year = 2019;
        int monthMay = 5;
        int readingValue = 50;
        Map<String, Integer> testMap1 = new HashMap<>();
        testMap1.put("year", year);
        Map<String, Integer> testMap2 = new HashMap<>();
        testMap2.put("May", readingValue);

        List<Map> testMapList = Arrays.asList(testMap1, testMap2);
        ResponseEntity testRespReturned = readingService.getSumOfReadingsByYearAndMonth(year, monthMay);
        List<Map> mapListReturned = (List<Map>) testRespReturned.getBody();

        assertThat(testMapList, equalTo(mapListReturned));
    }

    @Test
    public void sumOfReadingsByYearWhenNonExistingYear() {
        int year = 2021;
        Map<String, Integer> testMap1 = new HashMap<>();
        testMap1.put("year", year);
        Map<String, Integer> testMap2 = new HashMap<>();
        testMap2.put("total", 0);

        List<Map> testMapList = Arrays.asList(testMap1, testMap2);
        List<Map> listReturned = readingService.getSumOfReadingsByYear(year);

        assertThat(testMapList, equalTo(listReturned));
        assertThat(testMapList.get(0).get("year"), equalTo(year));
        assertThat(testMapList.get(1).get("total"), equalTo(0));
    }

    @Test
    public void monthlySumWhenNonExistingYear() {
        int year = 2021;

        YearlyReadingsInMonthsDto testYRIMD = new YearlyReadingsInMonthsDto(2021, new ArrayList<>(
                Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
        ));
        YearlyReadingsInMonthsDto yRIMDReturned = readingService.getMonthlySumOfReadingsByYear(year);

        assertThat(testYRIMD, equalTo(yRIMDReturned));
    }

    @Test
    public void sumOfReadingsByYearAndMonthWhenNonExistingYearAndMonth() {
        int year = 2021;
        int monthMay = 5;
        int readingValue = 0;
        Map<String, Integer> testMap1 = new HashMap<>();
        testMap1.put("year", year);
        Map<String, Integer> testMap2 = new HashMap<>();
        testMap2.put("May", readingValue);

        List<Map> testMapList = Arrays.asList(testMap1, testMap2);
        ResponseEntity testRespReturned = readingService.getSumOfReadingsByYearAndMonth(year, monthMay);
        List<Map> mapListReturned = (List<Map>) testRespReturned.getBody();

        assertThat(testMapList, equalTo(mapListReturned));
    }

    @TestConfiguration
    static class ReadingServiceTestContextConfiguration {

        @Bean
        public ReadingService readingService() {
            return new ReadingService();
        }
    }
}

