# Energy meter

An energy consumption processing application that is capable of storing metering data, adding new clients with meters and returning stored data.


## Table of contents
* [Technologies](#technologies)
* [Prerequisites](#prerequisites)
* [Packaging the app and running Jar](#packaging-the-app-and-running-jar)
* [Build](#build)
* [Running the application](#running-the-application)
* [Usage](#usage)
* [Examples](#examples)

## Technologies:
---

- **Maven** - Dependency Management

- **JDK** - Java™ Platform, Standard Edition Development Kit

- **Spring Boot** - Framework to ease the bootstrapping and development of new Spring Applications

- **H2** - An in-memory database

- **IntelliJIdea** -  A Java integrated development environment (IDE)

- **Insomnia** - Insomnia is a cross-platform GraphQL and REST client



## Prerequisites:
---
 - Java SDK 1.8 or later
 - Maven (built with 3.5)



## Packaging the app and running Jar
---
- Download the zip or clone the Git repository.
- Unzip the zip file (if you downloaded one)
- Open Command Prompt and Change directory (cd) to folder containing pom.xml
- From the command line run:
```
./mvnw package
``` 
- In the target directory, you should see `itsolutionstask-0.0.1-SNAPSHOT.jar`
- You can run the application now with the jar with:  
```shell
java -jar target/itsolutionstask-0.0.1-SNAPSHOT.jar
```


## Build
---
- Download the zip or clone the Git repository.
- Unzip the zip file (if you downloaded one)
- Open Command Prompt and Change directory (cd) to folder containing pom.xml
- From the command line run:
```
./mvnw install
``` 



## Running the application
---

- Download the zip or clone the Git repository.
- Unzip the zip file (if you downloaded one)
- Open Command Prompt and Change directory (cd) to folder containing pom.xml
    - Open Eclipse 
        - File -> Import -> Existing Maven Project -> Navigate to the folder where you unzipped the zip
        - Select the project
        - Choose the Spring Boot Application file (search for `@SpringBootApplication`)
        - Right Click on the file and Run as Java Application
    - OR
    - Open IntelliJ
        - File -> Open -> Navigate to the folder where you  have the pom.xml in the unzipped folder
        - Select pom.xml and click OK
        - Select Open as project
        - Open ItsolutionstaskApplication in project explorer
        - Right click on main method and select  '*Run ItsolutionstaskApplication.main()*'

        

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
./mvnw spring-boot:run
```

## Usage
---

Send your requests to
`localhost:8080` + the endpoints below:

GET Methods

| ENDPOINT | HTTP METHOD          | RESULT |
| ----------- | --------------- | ----- |
| /readings/monthsbyyear?year=`[int]`  | GET | Aggregated meter values for a whole year in monthly breakdown. |
| /readings/singlemonth?year=`[int]`&month=`[int]`| GET           | A sum of meter values for a specific month in a specific year.|
| /readings/yearlysum?year=`[int]` | GET          | Aggregated value for a whole year.|

POST Methods

| ENDPOINT | HTTP METHOD          |
| ----------- | --------------- |
| /readings?year=`[int]`&month=`[int]`&value=`[int]`&meterid=`[int]` | POST          |
| /clients?clientname=`[Str]`&identitycardnumber=`[Str]`&city=`[Str]`&zipcode=`[int]`&street=`[Str]`&housenumber=`[int]` | POST           |

**Enter months from 1-12 as in January=1,February=2 etc.**


## Examples
---

Aggregated readings for a year: 
[http://localhost:8080/readings/yearlysum?year=2018](http://localhost:8080/readings/yearlysum?year=2018)

Single months readings by year and month: 
[http://localhost:8080/readings/singlemonth?year=2018&month=5](http://localhost:8080/readings/singlemonth?year=2018&month=5)

Yearly readings in monthly breakdown: 
[http://localhost:8080/readings/monthsbyyear?year=2018](http://localhost:8080/readings/monthsbyyear?year=2018)

In case of no measuring data for given date: 
[http://localhost:8080/readings/yearlysum?year=2017](http://localhost:8080/readings/yearlysum?year=2017)

In case of wrong input of month: 
[http://localhost:8080/readings/singlemonth?year=2018&month=14](http://localhost:8080/readings/singlemonth?year=2018&month=14)



